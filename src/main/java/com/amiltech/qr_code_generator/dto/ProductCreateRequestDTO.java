package com.amiltech.qr_code_generator.dto;

import com.amiltech.qr_code_generator.constant.CategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreateRequestDTO {
    String name;
    Integer price;
    CategoryEnum category;
    String description;
}
