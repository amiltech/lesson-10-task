package com.amiltech.qr_code_generator.model;

import com.amiltech.qr_code_generator.constant.CategoryEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true,name = "name")
    String name;

    Integer price;

    @Enumerated(EnumType.STRING)
    CategoryEnum category;

    String description;
}
