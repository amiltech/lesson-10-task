package com.amiltech.qr_code_generator.service.impl;

import com.amiltech.qr_code_generator.dto.ProductCreateRequestDTO;
import com.amiltech.qr_code_generator.model.Product;
import com.amiltech.qr_code_generator.repository.ProductionProjection;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {
    Integer createProduct(ProductCreateRequestDTO dto);

    List<Product> getAllProducts();

    List<Product> getAllProductsFilterByPrice(Integer lowerPrice, Integer higherPrice);

    List<ProductionProjection> getProductCountByCategory();

    Page<Product> findAllProductsBySorting(Integer pageNum, Integer pageSize, String propertyName);
}