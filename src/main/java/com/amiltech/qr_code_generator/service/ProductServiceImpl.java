package com.amiltech.qr_code_generator.service;

import com.amiltech.qr_code_generator.dto.ProductCreateRequestDTO;
import com.amiltech.qr_code_generator.model.Product;
import com.amiltech.qr_code_generator.repository.ProductRepository;
import com.amiltech.qr_code_generator.repository.ProductionProjection;
import com.amiltech.qr_code_generator.service.impl.ProductService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Integer createProduct(ProductCreateRequestDTO dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setPrice(dto.getPrice());
        product.setCategory(dto.getCategory());
        Product createdProduct = productRepository.save(product);
        return createdProduct.getId();
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getAllProductsFilterByPrice(Integer lowerPrice, Integer higherPrice) {
        return productRepository.filterByPrice(lowerPrice,higherPrice);
    }
//
//    public Map<CategoryEnum, Long> getProductCountByCategory2() {
//        List<Object[]> results = productRepository.countProductsByCategory();
//        Map<CategoryEnum, Long> productCountMap = new HashMap<>();
//        for (Object[] result : results) {
//            CategoryEnum category = (CategoryEnum) result[0];
//            Long count = (Long) result[1];
//            productCountMap.put(category, count);
//        }
//        return productCountMap;
//    }

    @Override
    public List<ProductionProjection> getProductCountByCategory() {
        return productRepository.countProductsByCategory();
    }

    @Override
    public Page<Product> findAllProductsBySorting(Integer pageNum,Integer pageSize,String propertyName) {
        Sort sort = Sort.by(Sort.Direction.ASC, propertyName);
        Pageable v = PageRequest.of(pageNum,pageSize,sort);
        Page<Product> all = productRepository.findAll(v);
        return all;
    }
}
