package com.amiltech.qr_code_generator.controller;

import com.amiltech.qr_code_generator.dto.ProductCreateRequestDTO;
import com.amiltech.qr_code_generator.model.Product;
import com.amiltech.qr_code_generator.repository.ProductionProjection;
import com.amiltech.qr_code_generator.service.impl.ProductService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/find-all")
    public ResponseEntity<List<Product>> findAllProducts() {
        List<Product> allProducts = productService.getAllProducts();
        return new ResponseEntity<>(allProducts, HttpStatus.OK);
    }

    @GetMapping("/find-all-by-filter")
    public ResponseEntity<List<Product>> findAllProductsByFilter(
            @RequestParam(required = false) Integer lowPrice,
            @RequestParam(required = false) Integer highPrice) {

        List<Product> allProductsByFilter = productService.getAllProductsFilterByPrice(lowPrice, highPrice);
        return new ResponseEntity<>(allProductsByFilter, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Integer> createProduct(@RequestBody ProductCreateRequestDTO dto) {
        Integer id = productService.createProduct(dto);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping("/read-products-by-category")
    public ResponseEntity<List<ProductionProjection>> readProductsByCategory() {
        List<ProductionProjection> productCountByCategory = productService.getProductCountByCategory();
        return new ResponseEntity<>(productCountByCategory, HttpStatus.OK);
    }

    @GetMapping("/read-products-by-sorting")
    public ResponseEntity<Page<Product>> readProductsBySorting(@RequestParam Integer pageNum, Integer pageSize, String propertyName) {
        Page<Product> allProductsBySorting = productService.findAllProductsBySorting(pageNum, pageSize, propertyName);
        return new ResponseEntity<>(allProductsBySorting, HttpStatus.OK);
    }
}
