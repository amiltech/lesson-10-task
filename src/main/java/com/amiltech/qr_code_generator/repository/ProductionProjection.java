package com.amiltech.qr_code_generator.repository;

import com.amiltech.qr_code_generator.constant.CategoryEnum;
import lombok.Data;

public interface ProductionProjection {
    CategoryEnum getCategory();
    Long getCount();
}
