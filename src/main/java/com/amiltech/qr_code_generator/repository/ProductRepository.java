package com.amiltech.qr_code_generator.repository;

import com.amiltech.qr_code_generator.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer>, PagingAndSortingRepository<Product, Integer> {
//    @Query("SELECT p FROM Product p WHERE p.price >= :lowPrice AND p.price <= :highPrice")
//    List<Product> filterByPrice(Integer lowPrice, Integer highPrice);

    @Query("SELECT p FROM Product p WHERE (:lowPrice IS NULL OR p.price >= :lowPrice) AND (:highPrice IS NULL OR p.price <= :highPrice)")
    List<Product> filterByPrice(@Param("lowPrice") Integer lowPrice, @Param("highPrice") Integer highPrice);


//
//    @Query("SELECT p.category, COUNT(p) FROM Product p GROUP BY p.category")
//    List<Object[]> countProductsByCategory2();

    @Query("SELECT p.category AS category, COUNT(p) AS count FROM Product p GROUP BY p.category")
    List<ProductionProjection> countProductsByCategory();

}
